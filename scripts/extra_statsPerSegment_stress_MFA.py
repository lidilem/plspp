###############
#
# extra_statsPerSegment_stress.py
#
# PLSPP_MFA version : tier "words" with extra columns in stressTable (F0min	F0max	F0sd	syllmfa_equals_syllnuclei)
#
# This script parse all the TextGrid files (and stressTable.csv) and make a big table with all words 
# (including target and not target words) with stress info when available, and make another table with
# stats for each audio file (nb of tokens, nb of target words, nb of words correctly stressed, duration)
#
# This script isn't implemented yet in the pipeline (.sh file). You will have to run it separately for now.
# python extra_statsPerSegment_stress.py path/to/TextGridFolder path/to/stressTable.csv path/to/output/wordTable.csv path/to/output/segmentTable.csv
#
# 0. Parse stressTable.csv file to get full info about stress words (ID, number of syllable candidates...)
# 1. Parse TextGrid files, make a big csv table with one word per line and stress info if present & make statistics for each segment
# 3. Export statistic for each segment (audio file)
#
# S. Coulange 2022-2023

import re, os, textgrids, sys
from parselmouth.praat import call

input_folder = sys.argv[1] # textgrids with tiers POS (first position), WOR, Nuclei
if not input_folder.endswith('/'): input_folder+='/'
stressTableFilePath = sys.argv[2] # path and file name pointing to stressTable.csv
output_file = sys.argv[3] # output file wordTable.csv
output_file2 = sys.argv[4] # output file segmentTable.csv

# Do the job for this number of files only (0=all)
test_limit = 0




##################################################
#
# 0. Parse stressTable.csv file to get full info about stress words (ID, number of syllable candidates...)
#
print("Putting in memory stressTable.csv...")
cpt = 0
segment2stressTable = {} # for each segment (file), list words with stress info
with open(stressTableFilePath,"r") as inf:
    inf.readline() # skip header

    # STRUCTURE OF stressTable.csv
    # 0 spk
    # 1 lab
    # 2 pos
    # 3 lenSyllxpos
    # 4 expectedShapes
    # 5 expectedShape
    # 6 observedShape
    # 7 globalDeciles
    # 8 F0shape
    # 9 dBshape
    # 10 durshape
    # 11 syllF0
    # 12 F0Deciles
    # 13 sylldB
    # 14 dBDeciles
    # 15 sylldur
    # 16 durDeciles
    # 17 F0min
    # 18 F0max
    # 19 F0sd
    # 20 syllmfa_equals_syllnuclei

    # 21 deb (17)
    # 22 fin (18)
    # 23 file (19)
    # 24 ID (20)

    for line in inf:
        line = line.strip()
        l = line.split(';')
        if len(l)==25:
            if re.sub(r"(.merged.pos)?.TextGrid","",l[23]) not in segment2stressTable.keys():
                segment2stressTable[re.sub(r"(.merged.pos)?.TextGrid","",l[23])] = []
            segment2stressTable[re.sub(r"(.merged.pos)?.TextGrid","",l[23])].append(l)
            cpt+=1
print("Done.",cpt,"entries in memory.")


##################################################
#
# 1. Parse TextGrid files, make a big csv table with one word per line and stress info if present & make statistics for each segment
#
segment2stat = {}
cpt = 0
with open(output_file, 'w') as outf:
    outf.write("file;spk;i;word;POS;start;end;lenSyllxpos;expectedShapes;expectedShape;observedShape;globalDeciles;F0shape;dBshape;durshape;F0Deciles;dBDeciles;durDeciles;F0min;F0max;F0sd;syllmfa_equals_syllnuclei;ID\n")

    # STRUCTURE OF OUTPUT FILE
    # 0 file
    # 1 spk
    # 2 i
    # 3 word
    # 4 POS
    # 5 start
    # 6 end
    # 7 lenSyllxpos
    # 8 expectedShapes
    # 9 expectedShape
    # 10 observedShape
    # 11 globalDeciles
    # 12 F0shape
    # 13 dBshape
    # 14 durshape
    # 15 F0Deciles
    # 16 dBDeciles
    # 17 durDeciles
    # F0min	
    # F0max	
    # F0sd	
    # syllmfa_equals_syllnuclei
    # 18 ID

    for file in os.listdir(input_folder):
        
        if test_limit!=0 and cpt>test_limit: break
        else: cpt+=1
        
        # READ INPUT TEXTGRID FILE
        print("Processing",file,"...")
        try:
            grid = textgrids.TextGrid(input_folder+file)
        except:
            print("Unable to open the file!!")
            continue

        tg = call("Read from file...", input_folder+file)

        file = re.sub(r"(.merged.pos_shape)?.TextGrid","",file)
        spk = re.sub(r"_\d+$","",file)

        wordTable = [] # List of all words of the current file

        # LOOP ON INTERVALLES
        nbToken = 0
        nbTargetWords = 0
        nbExpIsObs = 0
        for i,intervalle in enumerate(grid['words']):
            lab = intervalle.text.transcode()
            
            if lab != "<p:>" and lab != "":
                nbToken+=1
                deb = round(intervalle.xmin,3)
                fin = round(intervalle.xmax,3)
                pos = grid['POS'][i].text.transcode()

                thisStressWord = ["" for x in range(0,25)] # Init to empty vector
                if file in segment2stressTable.keys():
                    for w in segment2stressTable[file]:
                        if round(float(w[21]),3) == deb:
                            thisStressWord = w # Get the stress info about this word if found in StressTable
                            nbTargetWords+=1

                            if w[9]==w[10]:
                                # Expected shape is observed (correct stress position)
                                nbExpIsObs+=1

                outf.write(';'.join([
                    format(file), # file
                    format(spk), # spk
                    format(i), # i
                    format(lab), # word
                    format(pos), # POS
                    format(round(deb,3)), # start
                    format(round(fin,3)), # end
                    format(thisStressWord[3]), # lenSyllxpos
                    format(thisStressWord[4]), # expectedShapes
                    format(thisStressWord[5]), # expectedShape
                    format(thisStressWord[6]), # observedShape
                    format(thisStressWord[7]), # globalDeciles
                    format(thisStressWord[8]), # F0shape
                    format(thisStressWord[9]), # dBshape
                    format(thisStressWord[10]), # durshape
                    format(thisStressWord[12]), # F0Deciles
                    format(thisStressWord[14]), # dBDeciles
                    format(thisStressWord[16]), # durDeciles
                    format(thisStressWord[17]),
                    format(thisStressWord[18]),
                    format(thisStressWord[19]),
                    format(thisStressWord[20]),
                    format(thisStressWord[24]), # ID
                    ])+"\n")

        ## Statistics on the current segment                    
        segment2stat[file] = {
            "spk":spk,
            "nbTokens":nbToken,
            "nbTargetWords":nbTargetWords,
            "nbExpIsObs":nbExpIsObs,
            "duration": round(grid['words'][-1].xmax,3)
        }
print(cpt,'files processed.')


##################################################
#
# 3. Export statistic for each segment (audio file)
#
print("GLOBAL STATISTICS PER SEGMENT:")
if len(segment2stat.keys())>0:
    print('file\t' + '\t'.join(list(segment2stat[list(segment2stat.keys())[0]].keys())))
else:
    print("No segment statistics.")

with open(output_file2, 'w') as outf:
    outf.write('file;' + ';'.join(list(segment2stat[list(segment2stat.keys())[0]].keys())) + '\n')
    for i,j in segment2stat.items():
        print(i, ";".join([str(y) for x,y in j.items()]))
        outf.write(format(i) + ";" + ";".join([format(y) for x,y in j.items()]) + "\n")
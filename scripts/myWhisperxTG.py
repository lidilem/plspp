###############
#
# myWhisperxTG.py
#
# For each wav file from input_folder, run WhisperX for speech recognition
# and word-level time alignment. Output format: TextGrid files.
#
# Arguments:
#   - path to input directory containing the audio files
#   - path to output directory for exporting TextGrid files
#   - Device to use
#   - Batch size
#   - Compute type
#   - Model of Whisper to use
#
# Adapted from https://github.com/m-bain/whisperX
# S. Coulange 2022-2024


import whisperx, sys, os, wave, contextlib, re
import signal

input_folder = sys.argv[1] # "audio/"
output_folder = sys.argv[2] # "whisperx/"
device = sys.argv[3] # "cuda" 
batch_size = int(sys.argv[4]) #16 # reduce if low on GPU mem
compute_type = sys.argv[5] # "int8" # change to "float16" if good GPU mem (better accuracy)
modelname = sys.argv[6] # "base.en"

class timeout:
    def __init__(self, seconds=1, error_message='Timeout'):
        self.seconds = seconds
        self.error_message = error_message

    def handle_timeout(self, signum, frame):
        raise TimeoutError(self.error_message)

    def __enter__(self):
        signal.signal(signal.SIGALRM, self.handle_timeout)
        signal.alarm(self.seconds)

    def __exit__(self, type, value, traceback):
        signal.alarm(0)


# transcribe with original whisper
model = whisperx.load_model(modelname, device, compute_type=compute_type)


for input_file in os.listdir(input_folder):
    print(f"Processing {input_file}...")
    result = ""
    try:
        with timeout(seconds=30):
            result = model.transcribe(input_folder+input_file, batch_size=batch_size)
    except:
        print("TIMEOUT!!")
        os.system("echo "+input_file+" >> bugsWhisperX.txt")
        continue

    #print(result["segments"]) # before alignment

    # load alignment model and metadata
    model_a, metadata = whisperx.load_align_model(language_code=result["language"], device=device)

    # align whisper output
    result_aligned = whisperx.align(result["segments"], model_a, metadata, input_folder+input_file, device)

    #print(result_aligned["segments"]) # after alignment
    #print(result_aligned["word_segments"]) # after alignment

    segs=[]
    for seg in result_aligned["word_segments"]:
        if 'start' in seg:
            segs.append([seg['start'], seg['end'], seg['word']])
            print(seg)
        else:
            print("Skip:",seg)

    print("\tGenerating the list of intervals...")
    intervals = []
    for indx, seg in enumerate(segs):

        if indx==0 and seg[0]>0: 
            intervals.append([0, seg[0], "<p:>"])
            intervals.append(seg)

        elif seg[0]>segs[indx-1][1]:
            # if xmin > precedent_xmax : create an empty interval
            intervals.append([segs[indx-1][1], seg[0], "<p:>"])
            intervals.append(seg)

        else:
            intervals.append(seg)

    print('\tGet corresponding wav file duration...')
    # Because we need to add an empty interval at the end of the file
    fname = input_folder + input_file
    with contextlib.closing(wave.open(fname,'r')) as f:
        frames = f.getnframes()
        rate = f.getframerate()
        duration = frames / float(rate)
        print(duration)

    print('\tGenerating TextGrid file...')

    if len(intervals)>0 and intervals[-1][1]<duration:
        intervals.append([intervals[-1][1], duration, "<p:>"])

    with open(output_folder+input_file.replace('.wav','.TextGrid'), 'w') as outf:
        outf.write('''File type = "ooTextFile"
Object class = "TextGrid"

xmin = 0 
xmax = {}
tiers? <exists> 
size = 1
item []:\n'''.format(duration))

        outf.write('''    item [1]:
        class = "IntervalTier"
        name = "WOR"
        xmin = 0
        xmax = {}
        intervals: size = {}\n'''.format(duration, len(intervals)))
        for i,seg in enumerate(intervals):
            outf.write('''        intervals [{}]:
            xmin = {}
            xmax = {}
            text = "{}"\n'''.format(i+1,seg[0],seg[1],re.sub(r'\.|,|\.\.\.|;|!|\?|…|"','',seg[2].lower())))


print("Done.")

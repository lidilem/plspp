import benepar, spacy, os, sys
# https://github.com/nikitakit/self-attentive-parser
# https://spacy.io/universe/project/self-attentive-parser

input_folder = sys.argv[1] # text file of raw transcript
output_dir = sys.argv[2]
benepar_model = sys.argv[3] # 'benepar_en3'
spacy_model = sys.argv[4] # 'en_core_web_md'

# Do the job for this number of files only (0=all)
test_limit = 0


##################################################
#
# 1. Load Spacy & Benepar
#

# Download BENEPAR model if doesn't exist
benepar.download(benepar_model)

# Load SPACY + BENEPAR
nlp = spacy.load(spacy_model)
if spacy.__version__.startswith('2'):
    nlp.add_pipe(benepar.BeneparComponent(benepar_model))
else:
    nlp.add_pipe("benepar", config={"model": benepar_model})


##################################################
#
# 2. Parse text files & run spacy+benepar
#

cpt = 0
for file in os.listdir(input_folder):
    
    if test_limit!=0 and cpt>test_limit: break
    else: cpt+=1
    
    text = ""
    print("Processing",file,"...")
    with open(input_folder+file, "r") as inf:
        text = inf.read()
        text = text.strip()

    doc = nlp(text)

    # Export output (plain text) with brackets instead of parenthesis
    with open(output_dir + file + ".benepar", "w") as outf:
        for sent in list(doc.sents):
            x = sent._.parse_string.replace("(","[").replace(")","]").replace("``","HYPH") # the last one is a common bug where `` should generally be HYPH
            #print(x)
            outf.write("{}\n".format(x))

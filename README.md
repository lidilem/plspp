# Pauses & Lexical Stress Processing Pipeline

This pipeline has been designed to annotate pause and lexical stress patterns in spontaneous non-native English speech. Initially intended to batch processing audio files from corpus containing mutliple files from multiple speakers, it can also be applicable to isolated speaker audio file(s). The outputs consist of a TextGrid file for each input audio file, containing a local analysis of lexical stress (example in the figure below). Additionally, the pipeline generates a `stressTable.csv` file that enumerates all occurrences of polysyllabic words along with details about their prosodic realization. Moreover, a `pauseTable.csv` file is produced, listing all empty intervals between words (which might be silent or filled) and information about their syntactic position, duration, etc. Pauses can be extracted from this list by specifying a duration threshold.

![Sample TextGrid view](scripts/sample_TextGridView.png)
Insight of a TextGrid output (from `shape/` folder) 

We developed a new version of the pipeline (`plspp_mfa.sh`), which adds a phoneme-level forced alignment step with the Montreal Forced Aligner (MFA). In this pipeline, syllabic prosodic features are extracted at the vowel level, rather than the syllable-nuclei position. F0 and intensity are extracted at 10 ms interval within each vowel segment (the time-step is customizable). Mean, min, max, and standard deviation of F0, and max intensity are computed. Duration corresponds to the vowel segment length. Syllable nuclei detection phase is preserved, but all polysyllabic words are included in the analysis, even if they do not contain the expected syllable nuclei count. A boolean value (`syllmfa_equals_syllnuclei`) for each target word indicates if the syllable nuclei count matches the number of aligned vowels. You can then filter words that does not match syllable nuclei count if you want. With this pipeline, you can also skip WhisperX speech recognition phase, and directly force-align a reference text if you are working with read-aloud speaking tasks.

Be aware that MFA struggles to align the transcription with long audio and disfluent speech. It might help if you segment your files in shorter ones (e.g. 30s~1min) if MFA alignment is struggling too much. For spontaneous speech with a lot of disfluencies, `plspp.sh` is more robust and usually claims better results.

![Sample TextGrid MFA view](scripts/mallory_0004pre_spctr.png)
Insight of a TextGrid output (from `shape/` folder) with `plspp_mfa.sh` pipeline

## Overview of the processing steps
- (Optional) neural speaker diarization with [Pyannote Audio](https://github.com/pyannote/pyannote-audio) if multispeaker audio files, 
- Automatic speech recognition and word level alignment with [WhisperX](https://github.com/m-bain/whisperX), 
- (only for plspp_mfa pipeline) Text output from WhisperX is forced aligned at word and phoneme level with [Montreal Forced Aligner](https://github.com/MontrealCorpusTools/Montreal-Forced-Aligner),
- Syllable nuclei detection with [Syllable-nuclei_v3](https://www.tandfonline.com/doi/full/10.1080/0969594X.2021.1951162), 
- Morphosyntactic analysis with [SpaCy](https://spacy.io/), and [Berkeley Neural Parser](https://spacy.io/universe/project/self-attentive-parser),
- Speaker normalization and F0, intensity, and duration features extraction at syllable level for each polysyllabic word that matches expected number of syllables,
- Comparison of prosodic shape of target words with canonical shape from [CMU Pronouncing Dictionary](http://www.speech.cs.cmu.edu/cgi-bin/cmudict), 
- Generation of the TextGrid files, stressTable.csv and pauseTable.csv files.

![Pipeline structure](scripts/plspp.png){width=500px}


## Installation

The pipeline needs to be ran on a GPU, at least for the speaker diarization and speech recognition phases.
The current setting runs fine on a Titan Pascal Xp, 12Go GPU memory, 128Go RAM, CUDA Version: 11.2.

```shell
# Clone this repo
git clone https://gricad-gitlab.univ-grenoble-alpes.fr/lidilem/plspp.git
cd plspp

# Create a virtual environment with all necessary packages # Python3.10.12
conda env create -f environment.yml
conda activate pipeline

# Make sure you have ffmpeg (WhisperX needs it)
sudo apt update && sudo apt install ffmpeg # If you don't have sudo rights, try without sudo, or proceed to a direct install: https://www.johnvansickle.com/ffmpeg/faq/

# Get server based Praat
# If version not found, check out https://www.fon.hum.uva.nl/praat/download_linux.html
# PRAAT VERSION IS OFTEN UPDATED !! ↓ the link below might not work anymore when you use plspp. Please update the version.
wget -c https://www.fon.hum.uva.nl/praat/praat6422_linux-intel64-barren.tar.gz -O - | tar -xz

# Get CMU dictionary
wget https://raw.githubusercontent.com/Alexir/CMUdict/master/cmudict-0.7b
mkdir CMU
mv cmudict-0.7b CMU/

# Create a folder in which you will put your audio files to process
mkdir audio

# IF YOU USE MFA PIPELINE
# Install MFA acoustic model and dictionary
mfa model download acoustic english_us_arpa
mfa model download dictionary english_us_arpa
```

## Usage

Put your audio files in wav format in the `audio/` folder, then run the pipeline.
```shell
bash plspp.sh
# OR
bash plspp_mfa.sh
```
Check out the [plspp](plspp.sh) or [plspp_mfa bash file](plspp_mfa.sh) for more details. Several arguments can be modified according to your needs.
Montreal Forced Aligner might ignore some files if beam is too narrow, you can add a `--beam` argument to the `mfa align` command. Default is `--beam 10`, you can try 20 or 50 or more, but processing time gets longer. Run the following command if you struggle to align your audio with MFA:
```bash
mfa validate audio/ english_us_arpa english_us_arpa --clean
```


If you need to speaker diarize your audio beforehand, put your multispeaker wave files in a folder `data/`, then run the following commands. You need a Hugging Face access token, that you can generate from [here](https://huggingface.co/settings/tokens).
```shell
mkdir pyannote
python scripts/diarisationPyannote.py data/ pyannote/ <HUGGINGFACE_ACCESS_TOKEN>

# Make TextGrid files from Pyannote's output.
# Arguments: pyannoteFilesFolder, audioFolder, silenceDurationThreshold
# below silenceDurationThreshold (in seconds), two consecutive speech segments from the same speaker are considered as one big segment.
python scripts/pyannote2TextGrid.py pyannote/ data/ 1

# Cut the original wav files in small wav files (one for each speech segment) for each speaker.
# Arguments: rawAudioFolder, pyannoteOutputFolder (in TextGrid format), outputFolder, maxNumberOfSpeakers, marginForAudioSegmentation (s), minimumDurationOfSpeechSegment (s)
# All segments shorter than minimumDurationOfSpeechSegment won't be exported.
./praat_barren scripts/intervalles2wav.praat ../data/ ../pyannote/ .wav.pyannote.TextGrid ../audio/ 3 0.01 8

# Then run the pipeline.
bash plspp.sh
```

## Visualization of lexical stress patterns
You can use [visualization/stress_viz.html](visualization/stress_viz.html) to visualize `stressTable.csv`. You will need to provide a list of speakers in CSV format, along with the `stressTable.csv` generated by the pipeline. The speaker file needs to have one column called "speakerID", listing all the speaker IDs to map with the speaker IDs from `stressTable.csv`. You may also have the following columns, which will be automatically filtered by the visualization tool: "Gender", "L1", "CLES" and "CLES_IO". "CLES" and "CLES_IO" were initially added for speaker proficiency levels, but you can put any factor variable in it. You will be able to easily filter them with the visualization tool.

Click on a word in the Word column to hear it, or on the speaker in the Speaker column to hear the word with a 4 seconds context. Open your browser's console to get more info about the triggered word.

A [Django server-based application for web hosting purposes](https://gricad-gitlab.univ-grenoble-alpes.fr/lidilem/plsppviz) is also available, with more features and visualisations.

|           |          |
| ---      | ---      |
| ![snapshot1](visualization/snapshot1.png) | ![snapshot2](visualization/snapshot2.png) |


## Limitations with the first pipeline "plspp.sh"
There are limitations with the initial pipeline `plspp.sh` at the current stage:
- WhisperX's word alignment tends to trim word edges to avoid noise, often leading to a shorter initial and final syllable.
- Syllable prosodic features are extracted at syllable nucleus level (syllable intenstity peak), without considering the F0 movement within the vowel. Syllable duration is extrapolated from the distance between syllable nuclei or word boundaries.

We try to solve these limitations with the new pipeline `plspp_mfa.sh`, which is still to be improved. The remaining limitations are:
- "Observed prosodic shape" is obtained by taking the mean of F0, intensity and duration features for each syllable in the word. All three prosodic dimensions have the same weight. It would be interesting to add an argument to ponderate freely all free dimensions, in order to give more weight to F0 for example.
- the new pipeline only deals with stress analysis at the moment.

## Improvements to be done
- Stress analysis of monosyllabic words (in progress)
- Sentence stress analysis (in progress)
- Make a parallel pipeline to compute stress and pause analysis on one single audio file, to get instant feedback after recording.

## Support
If you need any help or if you have any question, feel free to [contact me](mailto:sylvain.coulange@univ-grenoble-alpes.fr).

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## License
![CCBYSA_logo](https://licensebuttons.net/l/by-sa/4.0/88x31.png) This pipeline is an open-source work under the licence [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/). Feel free to use and adapt it to your needs!

# Citation
Please cite [this paper](https://www.mdpi.com/2226-471X/9/3/78) if you use or adapt the pipeline:
```bibtex
@Article{ CoulangeAl2024,
    AUTHOR = {Coulange, Sylvain and Kato, Tsuneo and Rossato, Solange and Masperi, Monica},
    TITLE = {Enhancing Language Learners’ Comprehensibility through Automated Analysis of Pause Positions and Syllable Prominence},
    JOURNAL = {Languages},
    VOLUME = {9},
    YEAR = {2024},
    NUMBER = {3},
    ARTICLE-NUMBER = {78},
    URL = {https://www.mdpi.com/2226-471X/9/3/78},
    ISSN = {2226-471X},
    DOI = {10.3390/languages9030078}
}

```
There is no paper dedicated to the pipeline description yet, but you can check out [this abstract](http://i3l.univ-grenoble-alpes.fr/~coulangs/doc/CoulangeAl2024_ISSP_abstract.pdf) and [this poster](http://i3l.univ-grenoble-alpes.fr/~coulangs/doc/CoulangeAl2024_ISSP_poster.pdf) presented at the 13th International Seminar on Speech Production (ISSP 2024), May 2024, Autrans, France.

# Studies using PLSPP or an adaptation of it
You may find further information about how the pipeline works and how it can be adapted in the following papers:
- [Coulange, S., Kato, T., Rossato, S., Masperi, M. (2024). Exploring Impact of Pausing and Lexical Stress Patterns on L2 English Comprehensibility in Real Time. Proceedings of Interspeech 2024, Sep. 2024, Kos, Greece.](https://www.isca-archive.org/interspeech_2024/coulange24_interspeech.html)
- [Nakanishi, M., Coulange, S. (2024). Measuring speech rhythm through automated analysis of syllabic prominences. Satellite Workshop of Speech Prosody "Prosodic features of language learners' fluency", July 1, Leiden, Netherlands.](http://i3l.univ-grenoble-alpes.fr/~coulangs/doc/NakanishiCoulange2024_SpeechProsodyWS.pdf)
- [Sugahara, M., Coulange, S., Kato, T. (2024). English Lexical Stress in Awareness and Production: Native and Non-native Speakers. The 19th Conference on Laboratory Phonology, June 27-29, Seoul, Korea. (in press)](https://labphon.org/sites/default/files/labphon19/Papers/LabPhon19_paper_117.pdf)
- [Coulange, S., Fries, M.-H., Masperi, M., Rossato, R. (2024). A corpus of spontaneous L2 English speech for real-situation speaking assessment. Proceedings of the 2024 Joint International Conference on Computational Linguistics, Language Resources and Evaluation (LREC-COLING 2024), 20-25 May, Torino, Italy.](https://aclanthology.org/2024.lrec-main.27/)
- [Kimura, T., Coulange, S., Kato, T. (2024). 日本人小学生による英語暗唱音声における語彙強勢位置の自動推定と母語話者評価 [Automatic estimation and native speakers’ evaluation of lexical stress positions in English recitation speech produced by Japanese elementary school children]. 日本音響学会第 151 回研究発表会 [Spring Meeting of the Acoustic Society of Japan], Mar. 2024, Tokyo, Japan. pp. 673-676](http://i3l.univ-grenoble-alpes.fr/~coulangs/doc/KimuraAl2024_ASJ_1-1-8_0300.pdf)
- [Sugahara, M., Coulange, S., Kato, T. (2023). 意識されている強勢 vs. 発話における強勢 ― 日本人と韓国人の大学生による英単語への主強勢付与の比較 [Stress awareness vs. stress production: Comparison of primary stress assignment to English words between Japanese and Korean university students]. 第 347 回日本音声学会研究例会 [347th regular meeting of the Phonetic Society of Japan], Nov 2023, online.](http://www.psj.gr.jp/jpn/regular-meeting/347th.html)
- [Coulange, S., Kato, T. (2023). Pause position analysis in spontaneous speech for L2 English fluency assessment. 日本音響学会第 150 回研究発表会 [Autumn Meeting of the Acoustical Society of Japan], Sep. 2023, Nagoya, Japan. pp. 991-994](https://hal.science/hal-04253964) (poster [here](https://hal.science/hal-04257261))
- [Coulange, S., Kato, T., Rossato, R., Masperi, M. (2023). フランス人学習者による自発 L2 英語発話における語彙アクセント自動測定 [Automatic Measurement of Lexical Stress in Spontaneous L2 English Speech of French Learners]. 第 37 回日本音声学会全国大会 [The 37th General Meeting of the Phonetic Society of Japan], Sep 2023, Sapporo, Japan. pp. 126-131](https://hal.science/hal-04253927)


This pipeline could not exist without the work of the contributors of [Pyannote Audio](https://github.com/pyannote/pyannote-audio), [WhisperX](https://github.com/m-bain/whisperX), [SpaCy](https://spacy.io/), [Syllable-nuclei_v3](https://www.tandfonline.com/doi/full/10.1080/0969594X.2021.1951162), [CMU Pronouncing Dictionary](http://www.speech.cs.cmu.edu/cgi-bin/cmudict), [Berkeley Neural Parser](https://spacy.io/universe/project/self-attentive-parser), and [Montreal Forced Aligner](https://github.com/MontrealCorpusTools/Montreal-Forced-Aligner). Thank you all for allowing the world to use your wonderful tools.
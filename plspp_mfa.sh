#!/bin/bash
#
# Pipeline for extracting pause and lexical stress features from speech
# This is PLSPP v2, with MFA implementation for word and phoneme level alignment.
# Input: audio wav files in folder audio/, file name will be considered as speaker name (you can add "_<number>" at the end of files if you have multiple files from the same speaker)
#           example: jan2023-201_089-009_SPEAKER_00_0.wav (speaker A: jan2023-201_089-009_SPEAKER_00)
#                    jan2023-201_089-009_SPEAKER_00_1.wav (speaker A: jan2023-201_089-009_SPEAKER_00)
#                    jan2023-201_089-009_SPEAKER_00_2.wav (speaker A: jan2023-201_089-009_SPEAKER_00)
#                    jan2023-202_053-128_SPEAKER_01_1.wav (speaker B: jan2023-202_053-128_SPEAKER_01)
#                    jan2023-202_053-128_SPEAKER_01_3.wav (speaker B: jan2023-202_053-128_SPEAKER_01)
#
# Outputs:
#       - folder shape/ with one TextGrid file per sound file, with tiers of parts-of-speech (POS), words (WOR), phonemes (phones), syllable-nuclei (Nuclei), expected prosodic shape (ExpectedShape), observed prosodic shape (ObservedShape), observed shape on F0, intensity and duration (DetailedShapes)
#       - stressTable.csv table of stress pattern per speaker: list of all target words (plurisyllabic plain words with appropriate number of syllables detected) and details about their pronunciation
#       - pauseTable.csv table of pauses per speaker: list of all word intersections (<p:> tags) with their duration and info about their syntactic position
#       - speakers.csv list of speakers
#       - other folders are temporary:
#           - whisperx/ : transcribed and word aligned sound files in TextGrid format
#           - syll/ : detection of syllable-nuclei as point tier in TextGrid format
#           - tg/ : combination of whisperx and syll files
#           - tgmfa/ : output from Montreal Forced Aligner (alignment at word and phoneme level of text/ and audio/ files)
#           - tgpos/ : tg files with parts-of-speech
#           - text/ : raw text file for each sound file (concatenation from whisperx files)
#           - benepar/ : constituency analysis of each text files
#        
#       - Sound files that couldn't be speech recognized by WhisperX are listed in bugsWhisperX.txt
#
# S. Coulange 2022-2024 sylvain.coulange@univ-grenoble-alpes.fr

echo Starting PLSPP pipeline!
echo This is PLSPP v2 - Keep in mind that alignment might not work well with disfluent speech.
echo Check MFA outputs. If alignment is too poor, use PLSPP v1.

#### ASR AND WORD ALIGNMENT WITH WHISPERX
echo Speech recognition and word alignment...
mkdir whisperx
python scripts/myWhisperxTG.py audio/ whisperx/ cuda 16 int8 base.en
# arguments: input_folder, output_folder, device, batch_size (reduce if low on GPU memory), compute_type (change to float16 if good GPU mem), model_name

# Get raw text from whisperx files
echo Get raw transcription files...
mkdir text
python scripts/textgrid2text.py whisperx/ audio/

#### SYLLABLE NUCLEI DETECTION
echo Syllable nuclei detection...
mkdir syll
./praat_barren scripts/SyllableNucleiv3_DeJongAll2021.praat ../audio/*.wav "Band pass (300..3300 Hz)" -25 2 0.3 "no" "English" 1.00 "TextGrid(s) only" "OverWriteData" "yes"
# arguments: list_of_input_files, option ("None", "Band pass (300..3300 Hz)", or "Reduce noise"), silence_threshold (dB), minimum_dip_near_peak (dB), minimum_pause_duration (s), filled_pause_detection ("yes" or "no"), language ("English" or "Dutch"), filled_pause_threshold, and some output options (cf. praat script)
mv audio/*.TextGrid syll/

#### MFA word & phoneme alignment
echo Word and phoneme alignment...
mkdir tgmfa
mfa align audio/ english_us_arpa english_us_arpa tgmfa/ --clean # Add --beam 20 or --beam 50 (or 100) if needed
mv audio/*.txt text/

echo Merging transcription, MFA alignement and syllable files...
mkdir tg
./praat_barren scripts/Merge_tiers_of_different_TextGrids.praat ../tgmfa/ ../syll/ '1-1,words/1-2,phones/2-1,Nuclei' ../tg/
# arguments: input_folderA, input_folderB, target tiers, output_folder

#### SYNTACTIC ANALYSIS WITH SPACY
echo Syntactic analysis...
mkdir tgpos
python scripts/spacyTextgrid_v2.py tg/ tgpos/ 'en_core_web_md' 'words'
# arguments: input_folder, output_folder, model_name, words_tier_name

#### LEXICAL STRESS ANALYSIS
echo Lexical stress pattern analysis...
mkdir shape
python scripts/stressAnalysis_mfa.py tgpos/ audio/ shape/ CMU/cmudict-0.7b
# arguments: textgrid_folder, audio_folder, output_folder, path_to_CMU_dictionary

#### PAUSE ANALYSIS
# echo Pause pattern analysis...
# mkdir benepar
# # Make constituency analysis from text files with Berkeley Neural Parser
# python scripts/text2benepar.py text/ benepar/ 'benepar_en3' 'en_core_web_md'
# # arguments: input_folder, output_folder, benepar_model_name, spacy_model_name
# # Run pause analysis
# python scripts/pausesAnalysis.py shape/ benepar/


echo Done!